package com.training.practice.soap.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PracticeSoapServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PracticeSoapServiceApplication.class, args);
	}

}

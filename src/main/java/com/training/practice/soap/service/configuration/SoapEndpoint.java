package com.training.practice.soap.service.configuration;

import com.training.soapservice.EmployeeResponse;
import com.training.soapservice.GetEmployeeRequest;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

//Mark Endpoint as Soap Endpoint
@Endpoint
public class SoapEndpoint {

    //request -> response
    @PayloadRoot(namespace = "http://training.com/soapservice", localPart = "GetEmployeeDetails")
    @ResponsePayload
    public EmployeeResponse configureSoapEndpoint(@RequestPayload GetEmployeeRequest getEmployeeRequest){
        EmployeeResponse response = new EmployeeResponse();
        EmployeeResponse.Employee employee = new EmployeeResponse.Employee();
        employee.setEmployeeId(getEmployeeRequest.getEmployeeId());
        employee.setEmployeeName("Krishna");
        employee.setEmployeeDesignation("Developer");
        response.setEmployee(employee);
        return response;
    }
}
